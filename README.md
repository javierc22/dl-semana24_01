## Semana 24 - Intro a Jquery y Yarn

1. Intro
2. Primer Javascript en Rails
3. Cargando Javascript en el Asset Path
5. CoffeeScript
6. Formas de integrar Jquery en Rails
7. Yarn
8. Backstretch
9. Backstretch después de cargar el DOM
10. Nuestro primer problema con Turbolinks
11. Turbolinks y Ajax
12. Cargando fotos del Asset Path
13. Intro a Datatable y Setup del proyecto
14. Integrando Datatables
15. Datatables con Bootstrap 4
16. Eliminando el doble Wrapper
17. Turbolinks en el Body vs Head

* Yarn: https://yarnpkg.com

Instalar un paquete con Yarn:
~~~
$ yarn add jquery
~~~

Instalar todos los paquetes que están en **yarn.lock**, los cuales son los que incluye el proyecto para funcionar con Yarn.
~~~
$ yarn install
~~~
